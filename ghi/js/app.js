function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
    <div class="card" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px; margin-bottom: 30px;">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <ul class="list">
        <li class="card-time">${starts}-${ends}</li>
      </ul>
    </div>
  `;
}
function alertComponent() {
  return `
  <div class="alert alert-primary" role="alert">
  There is something wrong!
  </div>
  `
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = new Date(details.conference.starts).toLocaleDateString();
          const ends = new Date(details.conference.ends).toLocaleDateString();
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, starts, ends, location);


          const column = document.querySelector(`#col-${index % 3}`);
          column.innerHTML += html;
          index += 1;
        }
      }

    }
  } catch (e) {
    console.error("You got an error!");

    const newHTML = alertComponent()
    const somethingWrong = document.querySelector('.something-wrong')
    somethingWrong.innerHTML = newHTML;
  }

});
