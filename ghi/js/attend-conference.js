console.log("hehe")
window.addEventListener('DOMContentLoaded', async () => {
  const url = `http://localhost:8000/api/conferences/`

  const response = await fetch(url)
  if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById("conference")
      for (let conference of data.conferences) {
        console.log(conference)
          let option = document.createElement("option")
          option.value = conference.href
          option.innerHTML = conference.name
          selectTag.appendChild(option)
      }
      // Here, add the 'd-none' class to the loading icon
      const selectSpinner = document.getElementById('loading-conference-spinner')
      selectSpinner.classList.add("d-none")
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove("d-none")
  }

  const formTag = document.getElementById('create-attendee-form')
  formTag.addEventListener('submit', async event => {
    event.preventDefault()
    const formData = new FormData(formTag)
    const json = JSON.stringify(Object.fromEntries(formData))


    const attendeeUrl = `http://localhost:8001/api/attendees/`
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(attendeeUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newAttendee = await response.json();

      }
      const selectSuccess = document.getElementById('success-message')
      selectSuccess.classList.remove("d-none")
      formTag.classList.add("d-none")
  })
})
